package com.example.user.mtrh;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class register extends Fragment {

    EditText myname,myid,mypass;
    Button save;
    DatabaseHelper db;
    TextView mylogin;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new DatabaseHelper(getContext(), "mtrhdb.sqlite", null, 1);
        db.querydata("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "type VARCHAR , name VARCHAR,email VARCHAR,phone VARCHAR,location VARCHAR,password VARCHAR)");
        myname = (EditText) getView().findViewById(R.id.name);
        myid = (EditText) getView().findViewById(R.id.patid);
        mypass = (EditText) getView().findViewById(R.id.password);
        save = (Button) getView().findViewById(R.id.btnsave);
        mylogin = (TextView) getView().findViewById(R.id.btnlogin);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    db.addpatient("",
                            myname.getText().toString().trim(),
                            myid.getText().toString().trim(),
                            mypass.getText().toString().trim()
                    );
                    Toast.makeText(getContext(), "Data successfully savedj", Toast.LENGTH_LONG).show();
                    //Toast.makeText(register.this, " Data Saved successfully", Toast.LENGTH_LONG).show();
                    myname.setText("");
                    mypass.setText("");
                    myid.setText("");

                } catch (Exception e) {
                    Toast.makeText(getContext(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_layout, container, false);
    }

    }

